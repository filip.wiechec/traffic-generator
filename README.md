# Traffic Generator

#### Description
Simple script sending roughly 10 requests per second
to target server.

The server should be specified by ip, it is also possible
to specify list of paths that will be joined to randomly
selected requests.

#### Example usage and output

```Bash
./steady_traffic_generator.py --ip 192.168.56.124 --paths / /downloads -t 3
Time elapsed: 3s        Requests sent: 27
```

Output:
```Bash
Statistics:

code | count
-----|------
200  | 27

The time is displayed in format that can be pasted directly to Grafana.

start:
2018-01-19 21:19:11

end:
2018-01-19 21:19:14
```

