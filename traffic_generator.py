#!/usr/bin/env python3
import argparse
import datetime
import logging
import re
import signal
import sys
import time
from collections import Counter
from random import randint

import requests
import requests.exceptions

TIME_FORMAT = '%Y-%m-%d %H:%M:%S'  # this is Grafana format


def gracefully_exit(signal, frame):
    print('It would have exited by itself after given time.')
    try:
        present_statistics(code_counter, start_time, stop_time)
    except:
        print('Something went wrong - no statistics to display. Is the IP correct?')
    exit(0)


signal.signal(signal.SIGINT, gracefully_exit)


def verify_url(url):
    re_verify_url = re.compile(
        r'^(?:http)?://'
        r'(?:localhost|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(?::\d+)?'
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)
    if re_verify_url.match(url):
        return True
    return False


def make_genuine_request(ip, paths=None):
    if isinstance(paths, list):
        path = paths[randint(0, len(paths) - 1)]
        url = 'http://{}{}'.format(ip, path)
    else:
        url = 'http://{}'.format(ip)

    if not verify_url(url):
        print('IP address or path not specified correctly.\n'
              'Run the script with --help argument for more details'
              'on correct format.')
        sys.exit(1)

    log = logging.getLogger('steady_traffic_generator')
    try:
        response = requests.get(url)
        return response.status_code
    except requests.exceptions.RequestException as e:
        log.warning(e)


def present_statistics(code_counter, start_time, stop_time):
    stats = []
    for status_code, count in code_counter.items():
        stats.append('{}  | {}'.format(status_code, count))
    print(
        '''

Statistics:

code | count
-----|------      
{}

The time is displayed in format that can be pasted directly to Grafana.

start:
{}

end:
{}
        '''.format('\n'.join(stats),
                   start_time.strftime(TIME_FORMAT),
                   stop_time.strftime(TIME_FORMAT)))


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--ip', nargs=1,
                        help='REQUIRED: target IP address',
                        metavar='<ip_address>')
    parser.add_argument('-p', '--paths', nargs='*',
                        help='whitespace separated paths to resources beginning with a slash', metavar='</path_to_resource>')
    parser.add_argument('-t', '--timeout', default='300',
                        help='for how many seconds should the script be '
                             'sending requests',
                        metavar='<seconds>')
    args = parser.parse_args()
    logging.basicConfig()

    start = time.time()
    start_time = datetime.datetime.now()  # laziness
    response_codes = []
    while (time.time() - start) < int(args.timeout):
        status_code = make_genuine_request(args.ip[0], args.paths)
        response_codes.append(status_code)
        time.sleep(0.1)
        sys.stdout.flush()
        code_counter = Counter(response_codes)
        print('Time elapsed: {}s\tRequests sent: {}'.format(int(time.time() - start), len(response_codes)), end='\r')
        stop_time = datetime.datetime.now()
    present_statistics(code_counter, start_time, stop_time)
